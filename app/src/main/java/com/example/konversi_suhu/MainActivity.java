package com.example.konversi_suhu;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private EditText xEdtCelcius,xEdtkelvin,xEdtfarenheit,xEdtReamur;
    private Button btnKonversi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Menghitung Suhu ");

        xEdtCelcius = (EditText) findViewById(R.id.edt_celcius);
        xEdtkelvin = (EditText) findViewById(R.id.edt_kelvin);
        xEdtfarenheit = (EditText) findViewById(R.id.edt_farenheit);
        xEdtReamur = (EditText) findViewById(R.id.edt_reamur);
        btnKonversi = (Button) findViewById(R.id.btn_konversi);

        btnKonversi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String celcius =xEdtCelcius.getText().toString().trim();


                double c = Double.parseDouble(celcius);

                double hslkelvin = c + 273.15;
                double hslfarenheit = c * 1.8 + 32;
                double hslreamur = c * 0.8;

                xEdtkelvin.setText(""+hslkelvin);
                xEdtfarenheit.setText(""+hslfarenheit);
                xEdtReamur.setText(""+hslreamur);

            }
        });



    }
}
